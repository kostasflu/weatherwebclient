/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teiath.weatherclient.resources;

/**
 *
 * @author Konstantinos
 */
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class ListTemp {
    
    private int day;
    private int min;
    private int max;
    private int night;
    private int eve;
    private int morn;

    public ListTemp() {
    }

    public ListTemp(int day, int min, int max, int night, int eve, int morn) {
        this.day = day;
        this.min = min;
        this.max = max;
        this.night = night;
        this.eve = eve;
        this.morn = morn;
    }

    public int getDay() {
        return day;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public int getNight() {
        return night;
    }

    public int getEve() {
        return eve;
    }

    public int getMorn() {
        return morn;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public void setNight(int night) {
        this.night = night;
    }

    public void setEve(int eve) {
        this.eve = eve;
    }

    public void setMorn(int morn) {
        this.morn = morn;
    }     
    
}
