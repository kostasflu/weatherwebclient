/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teiath.weatherclient.resources;

/**
 *
 * @author Konstantinos
 */
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.ArrayList;
import java.util.List;
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherList {

    private int dt;
    private ListTemp temp;
    private int pressure;
    private int humidity;
    //private ListWeather weather;
    List<ListWeather> weather = new ArrayList<>();
    private int speed;
    private int deg;
    private int clouds;

    public WeatherList() {
    }

    public WeatherList(int dt, ListTemp temp, int pressure, int humidity, int speed, int deg, int clouds) {
        this.dt = dt;
        this.temp = temp;
        this.pressure = pressure;
        this.humidity = humidity;
        this.speed = speed;
        this.deg = deg;
        this.clouds = clouds;
    }

    public int getDt() {
        return dt;
    }

    public ListTemp getTemp() {
        return temp;
    }

    public int getPressure() {
        return pressure;
    }

    public int getHumidity() {
        return humidity;
    }

    public List<ListWeather> getWeather() {
        return weather;
    }

    public int getSpeed() {
        return speed;
    }

    public int getDeg() {
        return deg;
    }

    public int getClouds() {
        return clouds;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public void setTemp(ListTemp temp) {
        this.temp = temp;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public void setWeather(List<ListWeather> weather) {
        this.weather = weather;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setDeg(int deg) {
        this.deg = deg;
    }

    public void setClouds(int clouds) {
        this.clouds = clouds;
    }

    
}
