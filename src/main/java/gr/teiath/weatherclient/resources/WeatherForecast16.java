/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teiath.weatherclient.resources;

import java.util.logging.Logger;

/**
 *
 * @author Konstantinos
 */
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.ArrayList;
import java.util.List;
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherForecast16 {
    
    
    private WeatherCity city;
    private int cod;
    private String message;
    private int cnt;
    //private WeatherList list;
    List<WeatherList> list = new ArrayList<>();

    public List<WeatherList> getList() {
        return list;
    }

    public void setList(List<WeatherList> list) {
        this.list = list;
    }


    
    
    
    public int getCod() {
        return cod;
    }

    public String getMessage() {
        return message;
    }

    public int getCnt() {
        return cnt;
    }

 

    public void setCod(int cod) {
        this.cod = cod;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    
    
    
    
    
    
    

    public WeatherForecast16() {
    }


    public void setCity(WeatherCity city) {
        this.city = city;
    }

    public WeatherCity getCity() {
        return city;
    }

    public WeatherForecast16(WeatherCity city) {
        this.city = city;
    }

}
