/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teiath.weatherclient.resources;

/**
 *
 * @author Konstantinos
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherCity {

    private int geoname_id;
    private String name;
    private int lat;
    private int lon;
    private String country;
    private String iso2;
    private String type;
    private int population;

    public WeatherCity() {
    }

    public WeatherCity(int geoname_id, String name, int lat, int lon, String country, String iso2, String type, int population) {
        this.geoname_id = geoname_id;
        this.name = name;
        this.lat = lat;
        this.lon = lon;
        this.country = country;
        this.iso2 = iso2;
        this.type = type;
        this.population = population;
    }

    public int getGeoname_id() {
        return geoname_id;
    }

    public String getName() {
        return name;
    }

    public int getLat() {
        return lat;
    }

    public int getLon() {
        return lon;
    }

    public String getCountry() {
        return country;
    }

    public String getIso2() {
        return iso2;
    }

    public String getType() {
        return type;
    }

    public int getPopulation() {
        return population;
    }

    public void setGeoname_id(int geoname_id) {
        this.geoname_id = geoname_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLat(int lat) {
        this.lat = lat;
    }

    public void setLon(int lon) {
        this.lon = lon;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setIso2(String iso2) {
        this.iso2 = iso2;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

   
}