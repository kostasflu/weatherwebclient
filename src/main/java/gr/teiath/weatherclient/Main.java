package gr.teiath.weatherclient;

import gr.teiath.weatherclient.resources.WeatherForecast16;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author amenychtas
 */
public class Main {

    //Example that shows the current weather from openweathermap.org
    //https://openweathermap.org/current
    public static void main(String[] args) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://api.openweathermap.org")
                .path("data/2.5/forecast/daily")
                .queryParam("q", "l1244")
                .queryParam("appid", "deee3ec181b16c60fc60fffdf4938822")
                .queryParam("units", "metric")
                .queryParam("cnt", "1")
                .queryParam("mode", "json");

        Response response = target
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
        WeatherForecast16 forecast16 = response.readEntity(WeatherForecast16.class);
        System.out.println(forecast16.getCod());
    }
}
