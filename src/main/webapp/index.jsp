<%-- 
    Document   : index.jsp
    Created on : Feb 2, 2017, 4:33:47 PM
    Author     : Konstantinos
--%>

<%@page import="gr.teiath.weatherclient.resources.ListWeather"%>
<%@page import="gr.teiath.weatherclient.resources.ListTemp"%>
<%@page import="gr.teiath.weatherclient.resources.WeatherList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page import="javax.ws.rs.core.Response"%>
<%@page import="gr.teiath.weatherclient.resources.WeatherForecast16"%>
<%@page import="javax.ws.rs.core.MediaType"%>
<%@page import="javax.ws.rs.client.WebTarget"%>
<%@page import="javax.ws.rs.client.ClientBuilder"%>
<%@page import="javax.ws.rs.client.Client"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Weather Forecast 16</title>
        <link href="css/bootstrap.css" rel="stylesheet">
    </head>
    <body>
        <%
            String previous_q = "";
            if (request.getParameter("q") != null) {
                previous_q = request.getParameter("q");
            }

        %>

        <form action="index.jsp" method="get" id="1" align="center" class="form-inline">
            <div class="form-group">
                <input type="text" name="q" class="form-control" value="<% out.print(previous_q); %>" placeholder="Enter City">
            </div>
            <div class="form-group">
                <select name="cnt" form_id="1" class="form-control">
                    <%                        for (int k = 1; k <= 16; k++) {
                            out.print("<option value='" + k + "'");
                            if (request.getParameter("cnt") != null & Integer.parseInt(request.getParameter("cnt")) == k) {
                                out.print(" selected ");
                            }
                            out.print(">" + k + "</option>");
                        }
                    %>

                </select>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>






        <%
            String q = request.getParameter("q");

            if (q != null) {

                int cnt = Integer.parseInt(request.getParameter("cnt"));

                Client client = ClientBuilder.newClient();
                WebTarget target = client.target("http://api.openweathermap.org")
                        .path("data/2.5/forecast/daily")
                        .queryParam("q", q)
                        .queryParam("appid", "deee3ec181b16c60fc60fffdf4938822")
                        .queryParam("units", "metric")
                        .queryParam("cnt", cnt)
                        .queryParam("mode", "json");

                Response response1 = target
                        .request()
                        .accept(MediaType.APPLICATION_JSON)
                        .get();
                WeatherForecast16 forecast16 = response1.readEntity(WeatherForecast16.class);

                if (forecast16.getCod() == 200) {
                    List<WeatherList> list = forecast16.getList();
                    WeatherList listi;

                    out.println("<h2 align='center'>" + forecast16.getCity().getName() + ",");
                    out.println(forecast16.getCity().getCountry() + "</h2>");
                    out.println("<br>");

                    out.println("<table class='table table-hover table-striped'>");
                    out.println("<tr>"
                            + "<th>Date</th>"
                            + "<th>Presure</th>"
                            + "<th>Humidity</th>"
                            + "<th>Speed</th>"
                            + "<th>Deg</th>"
                            + "<th>Clouds</th>"
                            + "<th>Temp</th>"
                            + "<th>Weather</th>"
                            + "</tr>");

                    for (int i = 0; i < list.size(); i++) {

                        listi = list.get(i);

                        out.println("<tr>");
                        out.println("<td>" + new Date(listi.getDt()) + "</td>");
                        out.println("<td>" + listi.getPressure() + "</td>");
                        out.println("<td>" + listi.getHumidity() + "</td>");
                        out.println("<td>" + listi.getSpeed() + "</td>");
                        out.println("<td>" + listi.getDeg() + "</td>");
                        out.println("<td>" + listi.getClouds() + "</td>");

                        ListTemp temp = listi.getTemp();
                        out.println("<td>"
                                + "Day :" + temp.getDay() + "<br>"
                                + "Eve :" + temp.getEve() + "<br>"
                                + "Max :" + temp.getMax() + "<br>"
                                + "Min :" + temp.getMin() + "<br>"
                                + "Morn :" + temp.getMorn() + "<br>"
                                + "Night :" + temp.getNight() + "<br>"
                                + "</td>");

                        List<ListWeather> weather = listi.getWeather();
                        ListWeather weatheri;
                        out.println("<td>");
                        out.println("<table class='table table-hover'>");

                        for (int j = 0; j < weather.size(); j++) {

                            weatheri = weather.get(j);

                            out.println("<tr>");
                            out.println("<td>"
                                    + "Id :" + weatheri.getId() + "<br>"
                                    + "Main :" + weatheri.getMain() + "<br>"
                                    + "Description :" + weatheri.getDescription() + "<br>"
                                    + "icon :" + "<img src='http://openweathermap.org/img/w/" + weatheri.getIcon() + ".png'>" + "<br>"
                                    + "</td>");
                            out.println("</tr>");
                        }
                        out.println("</table>");
                        out.println("</td>");
                        out.println("</tr>");
                    }
                    out.println("</table>");

                } else {
                    out.println("<div class='alert alert-danger' align='center'><strong > " + forecast16.getCod() + " - </strong >" + forecast16.getMessage() + " </div >");

                }

            }

        %>
    </body>
</html>
